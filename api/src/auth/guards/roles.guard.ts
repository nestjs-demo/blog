import { CanActivate, ExecutionContext, Injectable,Inject,forwardRef } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { UserService } from '../../user/service/user.service';
import { User } from 'src/user/models/user.interface';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        private reflector: Reflector,
        // @Inject(forwardRef(() => UserService))
        private userService:UserService
    ) {}

    canActivate(context: ExecutionContext):boolean | Promise<boolean> | Observable<boolean> {
        const roles = this.reflector.get<string[]>('roles',context.getHandler());
        if(!roles) {
        return true;
        }

        const request = context.switchToHttp().getRequest();
        const user = request.user.user;
        const r = request.user;
        // console.log(r);
        return this.userService.getSingleUser(user.id).pipe(
            map((user: User)=>{
                const hasRole = ()=> roles.indexOf(user.role)> -1;

                let hasPremission: boolean = false;

                if(hasRole()) {
                    hasPremission = true;
                    console.log(`has premission ${hasPremission}`);
                }
                return user && hasPremission;
            }));
    }


}