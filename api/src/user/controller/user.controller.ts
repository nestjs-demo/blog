import {
  Controller,
  Get,
  Param,
  Query,
  Body,
  Post,
  Put,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UserService } from '../service/user.service';

import { User, UserRole } from '../models/user.interface';
import { hasRoles } from 'src/auth/decorators/roles.decorators';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Pagination } from 'nestjs-typeorm-paginate';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  insertUser(@Body() user: User): Observable<User | Object> {
    return this.userService.insertUser(user).pipe(
      map((user: User) => user),
      catchError((err) => of({ error: err.message })),
    );
  }

  @Post('login')
  login(@Body() user: User): Observable<Object> {
    return this.userService.logIn(user).pipe(
      map((jwt: string) => {
        return { access_token: jwt };
      }),
      catchError((err) => of({ error: err.message })),
    );
  }


  getAllUsers(): Observable<User[]> {
    return this.userService.getUsers();
  }

  @Get()
  index(
    @Query('page') page: number =1,
    @Query('limit') limit: number = 10,
  ): Observable<Pagination<User>> {
    limit = limit > 100 ? 100 : limit;
    return this.userService.paginat({
      page: Number(page),
      limit: Number(limit),
      route: 'http://localhost:3000/users',
    });
  }

  @Get(':id')
  getUser(@Param('id') id: string): Observable<User | Object> {
    return this.userService
      .getSingleUser(Number(id))
      .pipe(catchError((err) => of({ error: err.message })));
  }

  @Put(':id')
  updateUser(@Param('id') id: number, @Body() user: User): any {
    return this.userService
      .updateUser(id, user)
      .pipe(catchError((err) => of({ error: err.message })));
  }

  @hasRoles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Put(':id/role')
  updateRoleofUser(
    @Param('id') id: string,
    @Body() user: User,
  ): Observable<User> {
    return this.userService
      .updateRoleofUser(Number(id), user)
      .pipe(catchError((err) => of({ error: err.message })));
  }

  @Delete(':id')
  deleteUser(@Param('id') id: any) {
    return this.userService
      .deleteUser(Number(id))
      .pipe(catchError((err) => of({ error: err.message })));
  }
}
