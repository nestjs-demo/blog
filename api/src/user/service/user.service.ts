import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User, UserRole } from '../models/user.interface';
import { UserEntity } from '../models/user.entity';
import { Repository } from 'typeorm';
import { from, Observable, of, throwError } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { AuthService } from 'src/auth/service/auth.service';
import {
  paginate,
  Pagination,
  IPaginationOptions,
} from 'nestjs-typeorm-paginate';

export class Util {
  static existValueInEnum(type: any, value: any): boolean {
    return Object.keys(type).filter(k => isNaN(Number(k))).filter(k => type[k] === value).length > 0;
  }
}

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private authservice: AuthService,
  ) {}


  insertUser(user: User): Observable<User | Object> {
    return this.authservice.hashPassword(user.password).pipe(
      switchMap((passwerdHash: string) => {
        const newUser = new UserEntity();
        newUser.name = user.name;
        newUser.username = user.username;
        newUser.email = user.email;
        newUser.role = UserRole.USER;
        newUser.password = passwerdHash;
        return from(this.userRepository.save(newUser)).pipe(
          map((user: User) => {
            const { password, ...resoult } = user;
            return resoult;
          }
          )
          ,catchError((err)=>throwError(err))
        );
      })
    );
  }

  getUsers(): Observable<User[]> {
    return from(this.userRepository.find()).pipe(
      map((users: User[]) => {
        users.forEach((user) => delete user.password);
        return users;
      }),
    );
  }
  
  paginat(options:IPaginationOptions):Observable<Pagination<User>> {
    return from(paginate(this.userRepository,options)).pipe(
      map((userPageable: Pagination<User>)=> {
        userPageable.items.forEach((v)=>{delete v.password});
        return userPageable;
      }
    ))
  }



  getSingleUser(id: number): Observable<User> {
    return from(this.userRepository.findOne({ id })).pipe(
      map((user: User) => {
        if (!user) {
          throw new NotFoundException('Could not find User');
        }
        const { password, ...resoult } = user;
        return resoult;
      })
      ,catchError((err) => throwError(err))
    );
  }

  deleteUser(id: number): Observable<any> {
    return from(this.userRepository.delete(id)).pipe(
      map((res) => {
        if (res.affected === 0) {
          throw new NotFoundException('Could not find User');
        }
        return { delete: 'success' };
      }),
    );
  }

  updateUser(id: number, user: User): Observable<any> {
    delete user.email;
    delete user.password;
    delete user.role;
    return from(this.userRepository.update(id, user)).pipe(
      map((res) => {
        if (res.affected === 0) {
          throw new NotFoundException('Could not Update User');
        }
        return null;
      }),
    );
  }

  updateRoleofUser(id:number, user: User):Observable<any> {
    if(!Util.existValueInEnum(UserRole,user.role)) {
      return throwError({message:`Roles Pattern: ${Object.values(UserRole)}`});
    }else {
      return from(
        this.userRepository.update(id, user)).pipe(
        map((state)=>{
        if(state.affected === 0) {
          throw new NotFoundException('Could not Update User');
        }else {
          const { password,...resoult } = user;
          return resoult;
        }
      })
    )
    }
  }

  logIn(user: User): Observable<string> {
    return this.validateUser(user.email, user.password).pipe(
      switchMap((user: User) => {
        if (user) {
          return this.authservice.generateJWT(user).pipe(map((jwt: string) => jwt));
        } else {
          return 'Wrong Credentials';
        }
      }),
    );
  }

  validateUser(email: string, password: string): Observable<User> {
    return this.findByEmail(email).pipe(
      switchMap((user: User) =>
        this.authservice.comparePasswords(password, user.password).pipe(
          map((match: boolean) => {
            if (match) {
              const { password, ...resoult } = user;
              return resoult;
            } else {
              throw new NotFoundException(`Invalid Password`)
            }
          })
          
        ),
      )
      ,catchError((err)=>throwError(err))
    );
  }



  findByEmail(email: string): Observable<User | Object> {
    return from(this.userRepository.findOne({ email })).pipe(map((user:User)=>{
      if(!user) {
        throw new NotFoundException(`Could not find Email`)
      }
      return user;
    })
    ,catchError((err)=>throwError(err))
    )
  }
}
